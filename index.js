const express = require('express');
const app = express();

const sort = (arr) => {
  for (let i = 1; i < arr.length; i++) {
    let key = arr[i];
    let j = i - 1;
    while (j >= 0 && key < arr[j]) { 
      arr[j + 1] = arr[j];
      j--;
    }
    arr[j + 1] = key;
  }
  return arr;
}

app.get('/sort', (req, res) => {
  // convert req.query.values to an array
  const values = req.query.values.split(',').map(val => parseInt(val));  
  // sends back the values *sorted* through the HTTP response
  res.send(sort(values).join(','));
})

app.get('/redirect', (req, res) => {
  const redirectUrl = req.query.redirectUrl;
  if (isValidRedirectUrl(redirectUrl)) {
    res.redirect(redirectUrl);
  } else {
    res.status(400).send('Invalid redirect URL');
  }
})

const server = app.listen(process.env.PORT || 3000, () => {
  console.log(`app listening on port ${process.env.PORT || 3000}!`);
});

// serve local files
app.use(express.static('public'));
app.use(express.static('dist'));

app.close = (done) => {
  server.close(done);
}

module.exports = app